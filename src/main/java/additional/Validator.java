package additional;

import java.util.regex.Pattern;

public class Validator {

    private static final String VALID_EMAIL_ADDRESS_REGEX = "^(.+)@(.+)\\.(.+)$";
    private static final String VALID_PHONE_NUMBER_REGEX = "^[3][7][5]\\d{2}\\s\\d{7}$";


    //375** ******* - format
    public static boolean validateEmail(String value) {
        boolean isMatch = Pattern.matches(VALID_EMAIL_ADDRESS_REGEX,value);
        return isMatch;
    }

    //*****@***.***  - format
    public static boolean validatePhoneNumber(String value) {
        boolean isMatch = Pattern.matches(VALID_PHONE_NUMBER_REGEX,value);
        return isMatch;
    }
}
