package com.itseliuk.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "employee", schema="testworkdb")
public class EmployeeEntity{

    public EmployeeEntity() {
    }

    public EmployeeEntity(Long id) {
        this.id = id;
    }

    public EmployeeEntity(String fullName, Timestamp dateOfBirth, String phoneNumber, String emailAdress, String position, Timestamp dateOfEmployment) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.emailAdress = emailAdress;
        this.position = position;
        this.dateOfEmployment = dateOfEmployment;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "date_of_birth", nullable = false)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Timestamp dateOfBirth;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(name = "email_adress", nullable = false)
    private String emailAdress;

    @Column(name = "position", nullable = false)
    private String position;

    @Column(name = "date_of_employment", nullable = false)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Timestamp dateOfEmployment;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "department_id", unique = false)
    private DepartmentEntity department;

    @JsonIgnore
    public void setDepartment(DepartmentEntity department) {
        this.department = department;
        department.setEmployee(this);
    }

    @JsonIgnore
    public void deleteDepartment() {
        department.deleteEmployee(this);
        department = null;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Timestamp getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Timestamp dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Timestamp getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(Timestamp dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(dateOfBirth, that.dateOfBirth) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(emailAdress, that.emailAdress) &&
                Objects.equals(position, that.position) &&
                Objects.equals(dateOfEmployment, that.dateOfEmployment) &&
                Objects.equals(department, that.department);
    }

}
