package com.itseliuk.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "department", schema="testworkdb")
public class DepartmentEntity{

    public DepartmentEntity() {
    }

    public DepartmentEntity(Long id) {
        this.id = id;
    }

    public DepartmentEntity(String name, String description, String phoneNumber, Timestamp dateOfFormation) {
        this.name = name;
        this.description = description;
        this.phoneNumber = phoneNumber;
        this.dateOfFormation = dateOfFormation;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "date_of_formation")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Timestamp dateOfFormation;

    @OneToMany(mappedBy = "department")
    private Set<EmployeeEntity> employee = new HashSet<>();

    @JsonIgnore
    public void setEmployee(EmployeeEntity employee) {
        this.employee.add(employee);
    }

    public void deleteEmployee(EmployeeEntity employee) {
        this.employee.remove(employee);
    }

    public Set<EmployeeEntity> getEmployee() {
        return employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Timestamp getDateOfFormation() {
        return dateOfFormation;
    }

    public void setDateOfFormation(Timestamp dateOfFormation) {
        this.dateOfFormation = dateOfFormation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentEntity that = (DepartmentEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(dateOfFormation, that.dateOfFormation) &&
                Objects.equals(employee, that.employee);
    }

}
