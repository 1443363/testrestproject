package com.itseliuk.services;

import java.util.List;

public interface DaoService<T, D> {

    List<T> getAll();

    T get(String id);

    void create(D item);

    void delete(String id);
}
