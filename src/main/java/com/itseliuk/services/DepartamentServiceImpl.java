package com.itseliuk.services;

import com.itseliuk.dto.DepartmentDTO;
import com.itseliuk.dto.HelperDTO;
import com.itseliuk.entities.DepartmentEntity;
import com.itseliuk.entities.EmployeeEntity;
import com.itseliuk.exceptions.IncorrectIdException;
import com.itseliuk.exceptions.NoRecordsException;
import com.itseliuk.repositories.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class DepartamentServiceImpl implements DaoService<DepartmentEntity, DepartmentDTO>{

    private DepartmentRepository repo;

    @Autowired
    public void setRepo(DepartmentRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<DepartmentEntity> getAll() {
        if (!repo.findAll().iterator().hasNext()) {
            try {
                throw new NoRecordsException("There are no records in Department table at the moment.");
            } catch (NoRecordsException e) {
                e.printStackTrace();
            }
        }
        return repo.findAll();
    }

    @Override
    public DepartmentEntity get(String id) {
        if (repo.findById(Long.valueOf(id)).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no department with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        return repo.findById(Long.valueOf(id)).get();
    }

    @Override
    public void create(DepartmentDTO item) {
        DepartmentEntity departmentEntity = HelperDTO.DTOToEntity(item);
        repo.save(departmentEntity);
    }

    @Override
    public void delete(String id) {
        if (repo.findById(Long.valueOf(id)).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no department with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        DepartmentEntity departmentEntity = repo.findById(Long.valueOf(id)).get();
        repo.delete(departmentEntity);
    }

    public Set<EmployeeEntity> getAllEmployeesFromDepartment(String id) {
        if (repo.findById(Long.valueOf(id)).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no user with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        DepartmentEntity departmentEntity = repo.findById(Long.valueOf(id)).get();
        Set<EmployeeEntity> employeesFromDepartment = departmentEntity.getEmployee();
        return employeesFromDepartment;
    }
}
