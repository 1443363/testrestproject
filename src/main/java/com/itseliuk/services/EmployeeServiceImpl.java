package com.itseliuk.services;

import additional.Validator;
import com.itseliuk.dto.EmployeeDTO;
import com.itseliuk.dto.HelperDTO;
import com.itseliuk.entities.DepartmentEntity;
import com.itseliuk.entities.EmployeeEntity;
import com.itseliuk.exceptions.IncorrectEmailFormatException;
import com.itseliuk.exceptions.IncorrectIdException;
import com.itseliuk.exceptions.IncorrectPhoneNumberFormatException;
import com.itseliuk.exceptions.NoRecordsException;
import com.itseliuk.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements DaoService<EmployeeEntity, EmployeeDTO>{

    private EmployeeRepository repo;
    private DepartamentServiceImpl departamentService;

    @Autowired
    public void setRepo(EmployeeRepository repo) {
        this.repo = repo;
    }

    @Autowired
    public void setDepartamentService(DepartamentServiceImpl departamentService) {
        this.departamentService = departamentService;
    }

    @Override
    public List<EmployeeEntity> getAll() {
        if (!repo.findAll().iterator().hasNext()) {
            try {
                throw new NoRecordsException("There are no records in Employee table at the moment.");
            } catch (NoRecordsException e) {
                e.printStackTrace();
            }
        }
        return repo.findAll();

    }

    @Override
    public EmployeeEntity get(String id) {
        if (repo.findById(Long.valueOf(id)).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no employee with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        return repo.findById(Long.valueOf(id)).get();
    }

    @Override
    public void create(EmployeeDTO employeeDTO) {
        EmployeeEntity employeeEntity = HelperDTO.DTOToEntity(employeeDTO);
        repo.save(employeeEntity);
    }

    public void addToDepartment(EmployeeDTO employeeDTO) {
        if (repo.findById(employeeDTO.getId()).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no employee with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        EmployeeEntity employeeEntity = repo.findById(employeeDTO.getId()).get();
        DepartmentEntity departmentEntity = departamentService.get(String.valueOf(employeeDTO.getDepartment().getId()));
        employeeEntity.setDepartment(departmentEntity);

        repo.save(employeeEntity);
    }

    public void removeFromDepartment(EmployeeDTO employeeDTO) {
        if (repo.findById(employeeDTO.getId()).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no employee with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        EmployeeEntity employeeEntity = repo.findById(employeeDTO.getId()).get();
        employeeEntity.deleteDepartment();
        repo.save(employeeEntity);
    }

    public List<EmployeeEntity> getAllWithoutDepartment() {
        List<EmployeeEntity> employees = repo.findAll();
        List<EmployeeEntity> employeesWithoutDepartment = employees.stream()
                .filter(e -> e.getDepartment() == null)
                .collect(Collectors.toList());
        if (employeesWithoutDepartment.isEmpty()) {
            try {
                throw new NoRecordsException("There are no employees in this department.");
            } catch (NoRecordsException e) {
                e.printStackTrace();
            }
        }

        return employeesWithoutDepartment;
    }

    @Override
    public void delete(String id) {
        if (repo.findById(Long.valueOf(id)).isEmpty()) {
            try {
                throw new IncorrectIdException("There is no employee with such id.");
            } catch (IncorrectIdException e) {
                e.printStackTrace();
            }
        }
        EmployeeEntity employeeEntity = repo.findById(Long.valueOf(id)).get();
        if (employeeEntity.getDepartment() != null) {
            employeeEntity.deleteDepartment();
        }
        repo.delete(employeeEntity);
    }
}
