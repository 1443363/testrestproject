package com.itseliuk.exceptions;

public class IncorrectEmailFormatException extends Exception{

    public IncorrectEmailFormatException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "IncorrectEmailFormatEception{}: " + getMessage();
    }

}