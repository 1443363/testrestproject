package com.itseliuk.exceptions;

public class IncorrectIdException extends Exception{
    public IncorrectIdException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "IncorrectIdException{}: " + getMessage();
    }
}