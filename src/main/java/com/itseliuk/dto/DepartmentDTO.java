package com.itseliuk.dto;

import additional.Validator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.itseliuk.exceptions.IncorrectPhoneNumberFormatException;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;


public class DepartmentDTO {

    private Long id;

    private String name;

    private String description;

    private String phoneNumber;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Timestamp dateOfFormation;

    private List<EmployeeDTO> employee;

    public void setEmployee(EmployeeDTO employee) {
        this.employee.add(employee);
        employee.setDepartment(this);
    }

    public List<EmployeeDTO> getEmployee() {
        return employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        boolean isValid = Validator.validatePhoneNumber(phoneNumber);
        if (isValid) {
            this.phoneNumber = phoneNumber;
        } else {
            try {
                throw new IncorrectPhoneNumberFormatException("Email should adhere to \" +\n" +
                        "                        \"the following format '375** *******' ");
            } catch (IncorrectPhoneNumberFormatException e) {
                e.printStackTrace();
            }
        }
        this.phoneNumber = phoneNumber;
    }

    public Timestamp getDateOfFormation() {
        return dateOfFormation;
    }

    public void setDateOfFormation(Timestamp dateOfFormation) {
        this.dateOfFormation = dateOfFormation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentDTO that = (DepartmentDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(dateOfFormation, that.dateOfFormation) &&
                Objects.equals(employee, that.employee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, phoneNumber, dateOfFormation, employee);
    }
}
