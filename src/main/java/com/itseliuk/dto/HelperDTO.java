package com.itseliuk.dto;

import com.itseliuk.entities.DepartmentEntity;
import com.itseliuk.entities.EmployeeEntity;

public class HelperDTO {

    public static EmployeeEntity DTOToEntity(EmployeeDTO employeeDTO) {
        EmployeeEntity employeeEntity = new EmployeeEntity(employeeDTO.getFullName(), employeeDTO.getDateOfBirth(),
                employeeDTO.getPhoneNumber(), employeeDTO.getEmailAdress(), employeeDTO.getPosition(),
                employeeDTO.getDateOfEmployment());

        return employeeEntity;
    }

    public static DepartmentEntity DTOToEntity(DepartmentDTO departmentDTO) {
        DepartmentEntity departmentEntity = new DepartmentEntity(departmentDTO.getName(),
                departmentDTO.getDescription(), departmentDTO.getPhoneNumber(), departmentDTO.getDateOfFormation());

        return departmentEntity;
    }


}
