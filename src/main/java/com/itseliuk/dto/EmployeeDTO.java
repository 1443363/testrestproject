package com.itseliuk.dto;

import additional.Validator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.itseliuk.exceptions.IncorrectEmailFormatException;
import com.itseliuk.exceptions.IncorrectPhoneNumberFormatException;

import java.sql.Timestamp;
import java.util.Objects;


public class EmployeeDTO {

    public EmployeeDTO() {
    }

    public EmployeeDTO(String fullName, Timestamp dateOfBirth, String phoneNumber, String emailAdress, String position, Timestamp dateOfEmployment) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.emailAdress = emailAdress;
        this.position = position;
        this.dateOfEmployment = dateOfEmployment;
    }

    public EmployeeDTO(String fullName, Timestamp dateOfBirth, String phoneNumber, String emailAdress, String position, Timestamp dateOfEmployment, DepartmentDTO department) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.emailAdress = emailAdress;
        this.position = position;
        this.dateOfEmployment = dateOfEmployment;
        this.department = department;
    }

    private Long id;
    private String fullName;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Timestamp dateOfBirth;
    private String phoneNumber;
    private String emailAdress;
    private String position;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Timestamp dateOfEmployment;
    private DepartmentDTO department;

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Timestamp getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Timestamp dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        boolean isValid = Validator.validatePhoneNumber(phoneNumber);
        if (isValid) {
            this.phoneNumber = phoneNumber;
        } else {
            try {
                throw new IncorrectPhoneNumberFormatException("Email should adhere to \" +\n" +
                        "                        \"the following format '375** *******' ");
            } catch (IncorrectPhoneNumberFormatException e) {
                e.printStackTrace();
            }
        }
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        boolean isValid = Validator.validateEmail(emailAdress);
        if (isValid) {
            this.emailAdress = emailAdress;
        } else {
            try {
                throw new IncorrectEmailFormatException("Email should adhere to " +
                        "the following format '*******@*****.***' ");
            } catch (IncorrectEmailFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Timestamp getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(Timestamp dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeDTO that = (EmployeeDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(dateOfBirth, that.dateOfBirth) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(emailAdress, that.emailAdress) &&
                Objects.equals(position, that.position) &&
                Objects.equals(dateOfEmployment, that.dateOfEmployment) &&
                Objects.equals(department, that.department);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, dateOfBirth, phoneNumber, emailAdress, position, dateOfEmployment, department);
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAdress='" + emailAdress + '\'' +
                ", position='" + position + '\'' +
                ", dateOfEmployment=" + dateOfEmployment +
                ", department=" + department +
                '}';
    }
}
