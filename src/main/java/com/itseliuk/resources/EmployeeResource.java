package com.itseliuk.resources;

import com.itseliuk.dto.EmployeeDTO;
import com.itseliuk.entities.EmployeeEntity;
import com.itseliuk.services.EmployeeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RestController
@RequestMapping("/api")
public class EmployeeResource {

    private EmployeeServiceImpl service;

    @Autowired
    public void setService(EmployeeServiceImpl service) {
        this.service = service;
    }

    ConcurrentMap<String, EmployeeEntity> empoyees = new ConcurrentHashMap<>();

    @GetMapping("/getAllEmployees")
    public List<EmployeeEntity> getAll() {
        return service.getAll();
    }

    @GetMapping("/getEmployeeById/{id}")
    public EmployeeEntity get(@PathVariable String id) {
        return service.get(id);
    }

    @PostMapping("/addEmployeeToDepartment")
    public void addEmployeeToDepartment(@RequestBody EmployeeDTO employee) {
        System.out.println(employee);
        service.addToDepartment(employee);
    }

    @PostMapping("/removeEmployeeFromDepartment")
    public void removeEmployeeFromDepartment(@RequestBody EmployeeDTO employee) {
        service.removeFromDepartment(employee);
    }

    @GetMapping("/getAllEmployeesWithoutDepartment")
    public List<EmployeeEntity> getAllEmployeesWithoutDepartment() {
        return service.getAllWithoutDepartment();
    }

    @PostMapping("/createEmployee")
    public void createEmployee(@RequestBody EmployeeDTO employee) {
        service.create(employee);
    }

    @GetMapping("/deleteEmployee/{id}")
    public void deleteEmployee(@PathVariable String id) {
        service.delete(id);
    }

}
