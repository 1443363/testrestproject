package com.itseliuk.resources;

import com.itseliuk.dto.DepartmentDTO;
import com.itseliuk.entities.DepartmentEntity;
import com.itseliuk.entities.EmployeeEntity;
import com.itseliuk.services.DepartamentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class DepartmentResource {

    private DepartamentServiceImpl service;

    @Autowired
    public void setService(DepartamentServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/getAllDepartments")
    public List<DepartmentEntity> getAll() {
        return service.getAll();
    }

    @GetMapping("/getDepartmentById/{id}")
    public DepartmentEntity get(@PathVariable String id) {
        return service.get(id);
    }

    @PostMapping("/createDepartment")
    public void createDepartment(@RequestBody DepartmentDTO department) {
        service.create(department);
    }

    @GetMapping("/deleteDepartment/{id}")
    public void deleteDepartment(@PathVariable String id) {
        service.delete(id);
    }

    @GetMapping("/getAllEmployeesFromDepartment/{id}")
    public Set<EmployeeEntity> getAllEmployeesFromDepartment(@PathVariable String id) {
        return service.getAllEmployeesFromDepartment(id);
    }

}
